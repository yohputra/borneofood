package com.example.borneofood.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.borneofood.R;
import com.example.borneofood.model.ProductModel;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder>{


    public interface ProductListenerCallback{
        void onSetFavoritePressed(int adapterPosition, String isLike);
        void onAddToCart(int adapterPosition, ProductModel productModel);
    }

    public static ProductListenerCallback productListenerCallback;
    private List<ProductModel> list;
    private Context context;

    public ProductAdapter(List<ProductModel> bannerList, ProductListenerCallback productListenerCallback) {
        this.list = bannerList;
        this.productListenerCallback = productListenerCallback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_product, parent, false);
        context = parent.getContext();
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ProductModel item = list.get(position);
        holder.context = context;
        holder.setClickListener(item);
        Log.d("is favorite ", item.getIs_favorite() + " " + item.getName());
        holder.tv_product_name.setText(item.getName());
        holder.tv_product_desc.setText(item.getDesc());
        holder.tv_product_price.setText(item.getNormal_price());
        holder.tv_product_price_disc.setText(item.getDisc_price());
        holder.tv_product_price_disc.setPaintFlags(holder.tv_product_price_disc.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        if(item.getIs_favorite().equals("0")){
            holder.iv_favorite.setImageResource(R.drawable.img_like_inactive);
        }else{
            holder.iv_favorite.setImageResource(R.drawable.img_like_active);
        }

        Picasso.get().load(item.getImg_path()).into(holder.iv_product);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView iv_product, iv_favorite,iv_min_qty, iv_plus_qty;
        TextView tv_product_name, tv_product_desc, tv_product_price, tv_product_price_disc;
        EditText et_qty;
        LinearLayout ln_qty;
        Button btn_add;
        Context context;

        ViewHolder(View itemView) {
            super(itemView);
            tv_product_name = itemView.findViewById(R.id.tv_product_name);
            tv_product_desc = itemView.findViewById(R.id.tv_product_desc);
            tv_product_price = itemView.findViewById(R.id.tv_product_price);
            tv_product_price_disc = itemView.findViewById(R.id.tv_product_price_disc);
            iv_product = itemView.findViewById(R.id.iv_product);
            iv_favorite = itemView.findViewById(R.id.iv_favorite);
            btn_add = itemView.findViewById(R.id.btn_add);
            ln_qty = itemView.findViewById(R.id.ln_qty);
            iv_min_qty = itemView.findViewById(R.id.iv_min_qty);
            iv_plus_qty = itemView.findViewById(R.id.iv_plus_qty);
            et_qty = itemView.findViewById(R.id.et_qty);
        }

        void setClickListener(final ProductModel item) {
            iv_favorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(item.getIs_favorite().equals("0")){
                        productListenerCallback.onSetFavoritePressed(getAdapterPosition(), "1");
                        iv_favorite.setImageResource(R.drawable.img_like_active);
                    }else{
                        productListenerCallback.onSetFavoritePressed(getAdapterPosition(), "0");
                        iv_favorite.setImageResource(R.drawable.img_like_inactive);
                    }

                }
            });

            btn_add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.setVisibility(View.GONE);
                    ln_qty.setVisibility(View.VISIBLE);

                    et_qty.setText(String.valueOf(item.getQty() + 1));
                    item.setQty(Integer.parseInt(et_qty.getText().toString()));
                    productListenerCallback.onAddToCart(getAdapterPosition(), item);
                }
            });

            iv_min_qty.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    et_qty.setText(String.valueOf(item.getQty() - 1));
                    item.setQty(Integer.parseInt(et_qty.getText().toString()));
                    productListenerCallback.onAddToCart(getAdapterPosition(), item);

                    if(item.getQty() == 0){
                        ln_qty.setVisibility(View.GONE);
                        btn_add.setVisibility(View.VISIBLE);
                    }

                }
            });

            iv_plus_qty.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    et_qty.setText(String.valueOf(item.getQty() + 1));
                    item.setQty(Integer.parseInt(et_qty.getText().toString()));
                    productListenerCallback.onAddToCart(getAdapterPosition(), item);
                }
            });
        }
    }
}
