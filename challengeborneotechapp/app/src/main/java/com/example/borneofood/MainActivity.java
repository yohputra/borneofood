package com.example.borneofood;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.borneofood.adapter.ProductAdapter;
import com.example.borneofood.adapter.ProductCategoryAdapter;
import com.example.borneofood.iontransport.IonMaster;
import com.example.borneofood.iontransport.ProductTransport;
import com.example.borneofood.model.ProductCategoryModel;
import com.example.borneofood.model.ProductModel;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.Future;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ProductAdapter.ProductListenerCallback {
    private RelativeLayout rl_parent_layout;
    private RecyclerView rvProductCategory, rv_product;
    private LinearLayout ln_floating;
    private NestedScrollView ns_parent;
    private TextView tv_qty_item, tv_total_price;
    private ProductCategoryAdapter productCategoryAdapter;
    private ProductAdapter productAdapter;

    private Future<JsonObject> productCategoryRequest;

    private List<ProductCategoryModel> listProductCategoryModel;
    private List<ProductModel> listProductModel;
    private int qtyItem = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initUi();
        initData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(productCategoryRequest == null){
            initData();
        }
    }


    private void initUi() {
        setContentView(R.layout.activity_main);

        rl_parent_layout = findViewById(R.id.rl_parent_layout);
        ns_parent = findViewById(R.id.ns_container);
        ln_floating = findViewById(R.id.ln_floating);
        rvProductCategory = findViewById(R.id.rv_product_category);
        rv_product = findViewById(R.id.rv_product);
        tv_qty_item = findViewById(R.id.tv_qty_item);
        tv_total_price = findViewById(R.id.tv_total_price);

        RecyclerView.LayoutManager layoutManagerHorizontal = new LinearLayoutManager(getBaseContext(), LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager layoutManagerVertical = new LinearLayoutManager(getBaseContext(), LinearLayoutManager.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getBaseContext(), layoutManagerVertical.getOrientation());

        rvProductCategory.setLayoutManager(layoutManagerHorizontal);
        rvProductCategory.setHasFixedSize(true);
        rvProductCategory.setNestedScrollingEnabled(true);
        rvProductCategory.setItemAnimator(new DefaultItemAnimator());

        rv_product.addItemDecoration(dividerItemDecoration);
        rv_product.setLayoutManager(layoutManagerVertical);
        rv_product.setHasFixedSize(true);
        rv_product.setNestedScrollingEnabled(true);
        rv_product.setItemAnimator(new DefaultItemAnimator());
    }

    private void initData() {
        listProductCategoryModel = new ArrayList<>();
        listProductModel = new ArrayList<>();

        productCategoryRequest = ProductTransport.getListProductCategory(this, new IonMaster.IonCallback() {
            @Override
            public void onReadyCallback(String errorMessage, Object object) {
                //progressBarBanner.setVisibility(View.GONE);

                if (errorMessage == null) {
                    JsonArray jsonArray = ((JsonObject) object).get("data").getAsJsonArray();
                    for (JsonElement jsonElement : jsonArray) {
                        JsonObject jsonObject = jsonElement.getAsJsonObject();
                        ProductCategoryModel productCategoryModel = new ProductCategoryModel();
                        productCategoryModel.parseProductCategory(jsonObject);
                        listProductCategoryModel.add(productCategoryModel);
                    }
                    productCategoryAdapter = new ProductCategoryAdapter(listProductCategoryModel);
                    rvProductCategory.setAdapter(productCategoryAdapter);
                }

                productCategoryRequest = null;
            }
        });

        Future<JsonObject> productRequest = ProductTransport.getListProduct(this, new IonMaster.IonCallback() {
            @Override
            public void onReadyCallback(String errorMessage, Object object) {
                //progressBarBanner.setVisibility(View.GONE);

                if (errorMessage == null) {
                    JsonArray jsonArray = ((JsonObject) object).get("data").getAsJsonArray();
                    for (JsonElement jsonElement : jsonArray) {
                        JsonObject jsonObject = jsonElement.getAsJsonObject();
                        ProductModel productModel = new ProductModel();
                        productModel.parseProduct(jsonObject);
                        listProductModel.add(productModel);
                    }
                    productAdapter = new ProductAdapter(listProductModel, MainActivity.this);
                    rv_product.setAdapter(productAdapter);
                }

                //bannerRequest = null;
            }
        });

    }

    @Override
    public void onSetFavoritePressed(int adapterPosition, String isLike) {
        listProductModel.get(adapterPosition).setIs_favorite(isLike);

        productAdapter.notifyDataSetChanged();

        Future<JsonObject> productCategoryRequest = ProductTransport.setFavoriteProduct(this, listProductModel.get(adapterPosition), new IonMaster.IonCallback() {
            @Override
            public void onReadyCallback(String errorMessage, Object object) {
                if (errorMessage == null) {
                    showToastFavorite(isLike);
                }
            }
        });


    }

    @Override
    public void onAddToCart(int adapterPosition, ProductModel productModel) {
        listProductModel.get(adapterPosition).setQty(productModel.getQty());

        qtyItem = 0;
        int totalHarga = 0;
        for (ProductModel item : listProductModel) {
            qtyItem += item.getQty();
            if(item.getQty() > 0){
                totalHarga += Integer.parseInt(item.getDisc_price()) * item.getQty();
            }

        }
        tv_qty_item.setText(String.valueOf(qtyItem));
        tv_total_price.setText(String.valueOf(totalHarga));

        setHeightNestedScrolview();
    }

    private void showToastFavorite(String isLike){
        final Snackbar snackbar = Snackbar.make(rl_parent_layout, "", Snackbar.LENGTH_LONG);
        View customSnackView = getLayoutInflater().inflate(R.layout.toast, null);
        snackbar.getView().setBackgroundColor(Color.TRANSPARENT);
        Snackbar.SnackbarLayout snackbarLayout = (Snackbar.SnackbarLayout) snackbar.getView();
        snackbarLayout.setPadding(20, 0, 20, 20);

        TextView tv_message_toast = customSnackView.findViewById(R.id.tv_message_toast);
        Button btn_see_favorited = customSnackView.findViewById(R.id.btn_see_favorited);
        if(isLike.equals("1")){
            tv_message_toast.setText("Berhasil ditambahkan ke Favoritku");
        }else{
            tv_message_toast.setText("Berhasil dihapus dari Favoritku");
        }
        btn_see_favorited.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                snackbar.dismiss();
                Intent intent = new Intent(getBaseContext(), FavoritkuActivity.class);
                startActivity(intent);
            }
        });
        snackbarLayout.addView(customSnackView, 0);
        snackbar.show();
    }

    private void setHeightNestedScrolview(){
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) ns_parent
                .getLayoutParams();
        if(qtyItem == 0){
            ln_floating.setVisibility(View.GONE);
            layoutParams.setMargins(0, 0, 0, 0);

        }else{
            ln_floating.setVisibility(View.VISIBLE);
            layoutParams.setMargins(0, 0, 0, 335);

        }
        ns_parent.setLayoutParams(layoutParams);
        ns_parent.setBackgroundColor(Color.WHITE);
    }
}