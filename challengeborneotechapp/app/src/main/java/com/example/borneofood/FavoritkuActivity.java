package com.example.borneofood;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.borneofood.adapter.ProductAdapter;
import com.example.borneofood.iontransport.IonMaster;
import com.example.borneofood.iontransport.ProductTransport;
import com.example.borneofood.model.ProductModel;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.Future;

import java.util.ArrayList;
import java.util.List;

public class FavoritkuActivity extends AppCompatActivity implements ProductAdapter.ProductListenerCallback {
    private ImageView iv_back_page;
    private RecyclerView rv_product;
    private ProductAdapter productAdapter;
    private List<ProductModel> listProductModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initUi();
        initData();
        initCntrl();
    }



    private void initUi() {
        setContentView(R.layout.activity_favoritku);
        iv_back_page = findViewById(R.id.iv_back_page);
        rv_product = findViewById(R.id.rv_product);

        LinearLayoutManager layoutManagerVertical = new LinearLayoutManager(getBaseContext(), LinearLayoutManager.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getBaseContext(), layoutManagerVertical.getOrientation());
        rv_product.addItemDecoration(dividerItemDecoration);
        rv_product.setLayoutManager(layoutManagerVertical);
        rv_product.setHasFixedSize(true);
        rv_product.setNestedScrollingEnabled(true);
        rv_product.setItemAnimator(new DefaultItemAnimator());
    }
    private void initData() {
        listProductModel = new ArrayList<>();

        Future<JsonObject> productRequest = ProductTransport.getProductFavorite(this, new IonMaster.IonCallback() {
            @Override
            public void onReadyCallback(String errorMessage, Object object) {
                //progressBarBanner.setVisibility(View.GONE);

                if (errorMessage == null) {
                    JsonArray jsonArray = ((JsonObject) object).get("data").getAsJsonArray();
                    for (JsonElement jsonElement : jsonArray) {
                        JsonObject jsonObject = jsonElement.getAsJsonObject();
                        ProductModel productModel = new ProductModel();
                        productModel.parseProduct(jsonObject);
                        listProductModel.add(productModel);
                    }
                    productAdapter = new ProductAdapter(listProductModel, FavoritkuActivity.this);
                    rv_product.setAdapter(productAdapter);
                }
            }
        });

    }

    private void initCntrl() {
        iv_back_page.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onSetFavoritePressed(int adapterPosition, String isLike) {

    }

    @Override
    public void onAddToCart(int adapterPosition, ProductModel productModel) {

    }
}