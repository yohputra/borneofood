package com.example.borneofood.iontransport;

import android.content.Context;
import android.util.Log;

import com.example.borneofood.R;
import com.example.borneofood.misc.Util;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.koushikdutta.async.future.FutureCallback;

import java.util.concurrent.CancellationException;

public class IonMaster {
    public interface IonCallback {
        void onReadyCallback(String errorMessage, Object object);
    }

    protected static FutureCallback<JsonObject> getJsonFutureCallback(final Context context, final IonCallback callback) {
        return new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject jsonObject) {
                if (e != null) {
                    if (e instanceof CancellationException) {
                        callback.onReadyCallback(context.getString(R.string.error_when_processing_your_data), e);
                    }
                    else if (Util.isNetworkAvailable(context)) {
                        callback.onReadyCallback(context.getString(R.string.error_when_processing_your_data), null);
                    }
                    else {
                        callback.onReadyCallback(context.getString(R.string.check_your_internet_connection), null);
                    }

                    Log.d("ION_MASTER", "onError: " + e.toString());

                } else {
                    Log.d("ION_MASTER", "onCompleted: " + jsonObject.toString());
                    callback.onReadyCallback(null, jsonObject);
                }
            }
        };
    }
}