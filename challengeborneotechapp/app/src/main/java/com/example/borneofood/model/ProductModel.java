package com.example.borneofood.model;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

public class ProductModel {
    private String id, name, merchant_id, category_id, desc, img_path, normal_price, disc_price, is_favorite;
    private int qty = 0;

    public String getId() {
        return id;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(String merchant_id) {
        this.merchant_id = merchant_id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImg_path() {
        return img_path;
    }

    public void setImg_path(String img_path) {
        this.img_path = img_path;
    }

    public String getNormal_price() {
        return normal_price;
    }

    public void setNormal_price(String normal_price) {
        this.normal_price = normal_price;
    }

    public String getDisc_price() {
        return disc_price;
    }

    public void setDisc_price(String disc_price) {
        this.disc_price = disc_price;
    }

    public String getIs_favorite() {
        return is_favorite;
    }

    public void setIs_favorite(String is_favorite) {
        this.is_favorite = is_favorite;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void parseProduct(JsonObject jsonObject){
        if (jsonObject.get("id") != null) {
            if (jsonObject.get("id") instanceof JsonPrimitive) {
                setId(jsonObject.get("id").getAsString().trim());

            } else {
                setId("");
            }

        } else {
            setId("");
        }

        if (jsonObject.get("merchant_id") != null) {
            if (jsonObject.get("merchant_id") instanceof JsonPrimitive) {
                setMerchant_id(jsonObject.get("merchant_id").getAsString().trim());

            } else {
                setMerchant_id("");
            }

        } else {
            setMerchant_id("");
        }

        if (jsonObject.get("category_id") != null) {
            if (jsonObject.get("category_id") instanceof JsonPrimitive) {
                setCategory_id(jsonObject.get("category_id").getAsString().trim());

            } else {
                setCategory_id("");
            }

        } else {
            setCategory_id("");
        }

        if (jsonObject.get("name") != null) {
            if (jsonObject.get("name") instanceof JsonPrimitive) {
                setName(jsonObject.get("name").getAsString().trim());

            } else {
                setName("");
            }

        } else {
            setName("");
        }

        if (jsonObject.get("desc") != null) {
            if (jsonObject.get("desc") instanceof JsonPrimitive) {
                setDesc(jsonObject.get("desc").getAsString().trim());

            } else {
                setDesc("");
            }

        } else {
            setDesc("");
        }

        if (jsonObject.get("img_path") != null) {
            if (jsonObject.get("img_path") instanceof JsonPrimitive) {
                setImg_path(jsonObject.get("img_path").getAsString().trim());

            } else {
                setImg_path("");
            }

        } else {
            setImg_path("");
        }

        if (jsonObject.get("normal_price") != null) {
            if (jsonObject.get("normal_price") instanceof JsonPrimitive) {
                setNormal_price(jsonObject.get("normal_price").getAsString().trim());

            } else {
                setNormal_price("");
            }

        } else {
            setNormal_price("");
        }

        if (jsonObject.get("disc_price") != null) {
            if (jsonObject.get("disc_price") instanceof JsonPrimitive) {
                setDisc_price(jsonObject.get("disc_price").getAsString().trim());

            } else {
                setDisc_price("");
            }

        } else {
            setDisc_price("");
        }

        if (jsonObject.get("is_favorite") != null) {
            if (jsonObject.get("is_favorite") instanceof JsonPrimitive) {
                setIs_favorite(jsonObject.get("is_favorite").getAsString().trim());

            } else {
                setIs_favorite("");
            }

        } else {
            setIs_favorite("");
        }
    }
}
