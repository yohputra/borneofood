package com.example.borneofood.model;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

public class ProductCategoryModel {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void parseProductCategory(JsonObject jsonObject){
        if (jsonObject.get("name") != null) {
            if (jsonObject.get("name") instanceof JsonPrimitive) {
                setName(jsonObject.get("name").getAsString().trim());

            } else {
                setName("");
            }

        } else {
            setName("");
        }
    }
}
