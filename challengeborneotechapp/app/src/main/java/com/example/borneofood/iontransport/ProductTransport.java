package com.example.borneofood.iontransport;

import android.content.Context;
import android.util.Log;

import com.example.borneofood.misc.Util;
import com.example.borneofood.model.ProductModel;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.Future;
import com.koushikdutta.ion.Ion;

public class ProductTransport extends IonMaster{

    public static Future<JsonObject> getListProductCategory(Context context, IonCallback callback){

        return Ion.with(context)
                .load(Util.getBaseUrl()+"getProductCategory")
                .setLogging("IONLOG", Log.DEBUG)
                .asJsonObject()
                .setCallback(getJsonFutureCallback(context, callback));
    }

    public static Future<JsonObject> getListProduct(Context context, IonCallback callback){

        return Ion.with(context)
                .load(Util.getBaseUrl()+"getProduct")
                .setLogging("IONLOG", Log.DEBUG)
                .asJsonObject()
                .setCallback(getJsonFutureCallback(context, callback));
    }

    public static Future<JsonObject> getProductFavorite(Context context, IonCallback callback){

        return Ion.with(context)
                .load(Util.getBaseUrl()+"getProductFavorite")
                .setLogging("IONLOG", Log.DEBUG)
                .asJsonObject()
                .setCallback(getJsonFutureCallback(context, callback));
    }

    public static Future<JsonObject> setFavoriteProduct(Context context, ProductModel productModel, IonCallback callback){

        return Ion.with(context)
                .load(Util.getBaseUrl()+"setFavoriteProduct")
                .setLogging("IONLOG", Log.DEBUG)
                .setMultipartParameter("id", productModel.getId())
                .setMultipartParameter("is_favorite", productModel.getIs_favorite())
                .asJsonObject()
                .setCallback(getJsonFutureCallback(context, callback));
    }
}
