package com.example.borneofood.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.borneofood.R;
import com.example.borneofood.model.ProductCategoryModel;

import java.util.ArrayList;
import java.util.List;

public class ProductCategoryAdapter extends RecyclerView.Adapter<ProductCategoryAdapter.ViewHolder>{
    private List<ProductCategoryModel> list = new ArrayList<>();
    private Context context;
    private int row_index = 0;

    public ProductCategoryAdapter(List<ProductCategoryModel> bannerList) {
        this.list = bannerList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_product_category, parent, false);
        context = parent.getContext();
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        ProductCategoryModel item = list.get(position);
        holder.context = context;

        holder.tv_product_category_name.setText(item.getName());

        holder.cl_product_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                row_index=position;
                notifyDataSetChanged();
            }
        });

        if(row_index == position){
            holder.tv_product_category_name.setBackground(ContextCompat.getDrawable(context, R.drawable.product_category_rounded_active));
        }else{
            holder.tv_product_category_name.setBackground(ContextCompat.getDrawable(context, R.drawable.product_category_rounded_inactive));
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ConstraintLayout cl_product_category;
        TextView tv_product_category_name;
        Context context;

        ViewHolder(View itemView) {
            super(itemView);
            tv_product_category_name = itemView.findViewById(R.id.tv_product_category_name);
            cl_product_category = itemView.findViewById(R.id.cl_product_category);
        }
    }
}
