$(document).ready(function () {
	$('#productForm').parsley();

	$('#productAdd').on('click', function (e) {
		$('#productModal').modal('show');
		$('#productForm').parsley().reset();
		$('#id').val("");
		$('#name').val("");

		$("#title_product_modal").text("Add Product");

		$('#preview_image').attr('style', 'display:none');
		$('#preview_image').attr('src', '');
		document.getElementById('image_source').required = true;
	});

	
	$('#productForm').on('submit', function (e) {
		e.preventDefault();
		var url;
		var form = $(this);
		form.parsley().validate();
		var formData = new FormData(this);
		

		if (form.parsley().isValid()) {
			$.ajax({
				url: base_url + 'admin/productcontroller/store',
				data: formData,
				type: 'POST',
				datatype: 'JSON',
				async: false,
				processData: false,
				contentType: false,
				beforeSend: function () {
					document.getElementById('rpModal').style.display = 'block';
				},
				success: function (data) {
					var data = JSON.parse(data);
					// console.log(data.status);
					document.getElementById('rpModal').style.display = 'none';
					if (data.status == 1) {
						Swal.fire({
							position: 'center',
							type: 'success',
							title: 'Data has been saved',
							showConfirmButton: false,
							timer: 1500
						}).then((timer) => {
							window.location.href = 'product';
						});
					} else {

						document.getElementById('rpModal').style.display = 'none';
						swal.fire(
							'Error',
							'Oops, your data was not saved.', 
							'error'
						)
					}


				},
				error: function (e) {

					document.getElementById('rpModal').style.display = 'none';

					swal.fire(
						'Error',
						'Oops, your data was not saved.', 
						'error'
					)
				}
			});
		}

	});

	/* Edit data table */
	$('#dataTable tbody').on('click', '#productEdit', function () {
		$('#productModal').modal('show');
		$('#productForm').parsley().reset();
		document.getElementById('image_source').required = false;
		$("#title_product_modal").text("Edit Product");

		var id = $(this).attr('data-value');
		$.ajax({
			url: base_url + 'admin/productcontroller/get/' + id,
			type: 'POST',
			beforeSend: function () {
				document.getElementById('rpModal').style.display = 'block';
			},
			success: function (response) {
				document.getElementById('rpModal').style.display = 'none';
				var data = JSON.parse(response);
				if (data.status == "1") {
					$('#id').val(data.data.id);
					$('#name').val(data.data.name);
					$('#desc').val(data.data.desc);
					$('#merchant_id').val(data.data.merchant_id);
					$('#category_id').val(data.data.category_id);
					$('#normal_price').val(data.data.normal_price);
					$('#disc_price').val(data.data.disc_price);

					$('#preview_image').attr('style', 'display:block');
					$('#orderby').val(data.data.orderby);
					$('#old_image').val(data.data.img_path);
					$('#preview_image').attr('src', base_url + '/assets/admin/upload/product/' + data.data.img_path);

				}
			},
			error: function (e) {

				document.getElementById('rpModal').style.display = 'none';
				swal.fire(
					'Error',
					'Oops, your data was not updated.', 
					'error'
				);
			}
		});
	});

});

function previewImage() {
	document.getElementById('preview_image').style.display = 'block';
	var oFReader = new FileReader();
	oFReader.readAsDataURL(document.getElementById('image_source').files[0]);

	oFReader.onload = function (oFREvent) {
		document.getElementById('preview_image').src = oFREvent.target.result;
	};
};