$(document).ready(function () {
	$('#categoryForm').parsley();

	$('#categoryAdd').on('click', function (e) {
		$('#categoryModal').modal('show');
		$('#categoryForm').parsley().reset();
		$('#id').val("");
		$('#name').val("");

		$("#title_category_modal").text("Add Category");
	});

	
	$('#categoryForm').on('submit', function (e) {
		e.preventDefault();
		var url;
		var form = $(this);
		form.parsley().validate();
		var formData = new FormData(this);
		

		if (form.parsley().isValid()) {
			$.ajax({
				url: base_url + 'admin/categorycontroller/store',
				data: formData,
				type: 'POST',
				datatype: 'JSON',
				async: false,
				processData: false,
				contentType: false,
				beforeSend: function () {
					document.getElementById('rpModal').style.display = 'block';
				},
				success: function (data) {
					var data = JSON.parse(data);
					// console.log(data.status);
					document.getElementById('rpModal').style.display = 'none';
					if (data.status == 1) {
						Swal.fire({
							position: 'center',
							type: 'success',
							title: 'Data has been saved',
							showConfirmButton: false,
							timer: 1500
						}).then((timer) => {
							window.location.href = 'category';
						});
					} else {

						document.getElementById('rpModal').style.display = 'none';
						swal.fire(
							'Error',
							'Oops, your data was not saved.', // had a missing comma
							'error'
						)
					}


				},
				error: function (e) {

					document.getElementById('rpModal').style.display = 'none';

					swal.fire(
						'Error',
						'Oops, your data was not saved.', // had a missing comma
						'error'
					)
				}
			});
		}

	});

	/* Edit data table */
	$('#dataTable tbody').on('click', '#categoryEdit', function () {
		$('#categoryModal').modal('show');
		$('#categoryForm').parsley().reset();
		
		$("#title_category_modal").text("Edit Category");

		var id = $(this).attr('data-value');
		$.ajax({
			url: base_url + 'admin/categorycontroller/get/' + id,
			type: 'POST',
			beforeSend: function () {
				document.getElementById('rpModal').style.display = 'block';
			},
			success: function (response) {
				document.getElementById('rpModal').style.display = 'none';
				var data = JSON.parse(response);
				if (data.status == "1") {
					$('#id').val(data.data.id);
					$('#name').val(data.data.name);

				}
			},
			error: function (e) {

				document.getElementById('rpModal').style.display = 'none';
				swal.fire(
					'Error',
					'Oops, your data was not updated.', // had a missing comma
					'error'
				);
			}
		});
	});
});