-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 09, 2023 at 07:26 PM
-- Server version: 10.4.25-MariaDB
-- PHP Version: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db-borneofood`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_category`
--

CREATE TABLE `t_category` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_category`
--

INSERT INTO `t_category` (`id`, `name`) VALUES
(1, 'Mie Manis'),
(2, 'Mie Pedas'),
(3, 'Mie Pedas'),
(4, 'Mie Pedas Super'),
(5, 'try657'),
(6, 'yuytuytu'),
(7, '324324 edit');

-- --------------------------------------------------------

--
-- Table structure for table `t_merchant`
--

CREATE TABLE `t_merchant` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_merchant`
--

INSERT INTO `t_merchant` (`id`, `name`) VALUES
(1, 'Mie Gacoan, Jalan Pasanah');

-- --------------------------------------------------------

--
-- Table structure for table `t_product`
--

CREATE TABLE `t_product` (
  `id` int(11) NOT NULL,
  `merchant_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `desc` text NOT NULL,
  `img_path` varchar(255) NOT NULL,
  `normal_price` decimal(10,0) NOT NULL,
  `disc_price` decimal(10,0) NOT NULL,
  `is_favorite` varchar(1) NOT NULL,
  `is_delete` varchar(1) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `modified_date` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_product`
--

INSERT INTO `t_product` (`id`, `merchant_id`, `category_id`, `name`, `desc`, `img_path`, `normal_price`, `disc_price`, `is_favorite`, `is_delete`, `created_date`, `created_by`, `modified_date`, `modified_by`) VALUES
(2, 1, 1, 'Mie Manis Ayam', 'Mie dengan rasa asin dan pedas, dengan ditaburi daging ayam cincang beserta pangsit yang kriuknya bikin nagih', 'product_63ba6f952dda3.png', '25000', '18000', '1', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(3, 1, 2, 'Mie manis sapi', 'Mie dengan rasa asin dan pedas, dengan ditaburi daging ayam cincang beserta pangsit yang kriuknya bikin nagih', 'product_63bc5a896d363.png', '20000', '30000', '0', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_category`
--
ALTER TABLE `t_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_merchant`
--
ALTER TABLE `t_merchant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_product`
--
ALTER TABLE `t_product`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_category`
--
ALTER TABLE `t_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `t_merchant`
--
ALTER TABLE `t_merchant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_product`
--
ALTER TABLE `t_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
