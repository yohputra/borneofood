<?php

class ProductModel extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    public function getData()
    {
        $this->db->select('t_product.*, t_category.name as name_cat, t_merchant.name as name_merchant ');  
        $this->db->join('t_category', 't_product.category_id = t_category.id');
        $this->db->join('t_merchant', 't_product.merchant_id = t_merchant.id');
        $query = $this->db->get('t_product');
        $result_array = $query->result_array();

        return $result_array;
    }

    public function storeData($post)
    {
        $datetime = date('Y-m-d H:i:s');

        if (!empty($_FILES['image_source']['name'])) {
            $this->db->select('*');
            $this->db->where('id', $post['id']);
            $query = $this->db->get('t_product');
            $result = $query->row_array();

            if (file_exists("./assets/admin/upload/product/" . $result['img_path'])) {
                unlink("./assets/admin/upload/product/" . $result['img_path']);
            }

            $image = $this->_uploadImage('product' . '_' . uniqid(), 'image_source');
            if ($image == '0') {
                return '0';
            }
        } else {
            $image = $post['old_image'];
        }


        if(!empty($post['id'])){
            $update_data = array(
                'merchant_id' => $post['merchant_id'],
                'category_id' => $post['category_id'],
                'name' => $post['name'],
                'desc' =>  $post['desc'],
                'img_path' => $image,
                'normal_price' => $post['normal_price'],
                'disc_price' => $post['disc_price'],
                'is_favorite' => '0',
                'is_delete' => '0',
            );
            $this->db->where('id', $post['id']);
            $store = $this->db->update('t_product', $update_data);
        }else{   
            $insert_data = array(
                'merchant_id' => $post['merchant_id'],
                'category_id' => $post['category_id'],
                'name' => $post['name'],
                'desc' =>  $post['desc'],
                'img_path' => $image,
                'normal_price' => $post['normal_price'],
                'disc_price' => $post['disc_price'],
                'is_favorite' => '0',
                'is_delete' => '0',
            );
            $store = $this->db->insert('t_product', $insert_data);
        }
        

        return $store;
    }

    public function getOneData($id)
    {
        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get('t_product');
        $result = $query->row_array();

        return $result;
    }

    public function storeFavoriteProduct($post)
    {
        $update_data = array(
            'is_favorite' => $post['is_favorite']
        );
        $this->db->where('id', $post['id']);
        $store = $this->db->update('t_product', $update_data);        

        return $store;
    }

    public function getProductFavorite()
    {
        $this->db->select('*');
        $this->db->where('is_favorite', "1");
        $query = $this->db->get('t_product');
        $result_array = $query->result_array();

        return $result_array;
    }


    private function _uploadImage($file_name, $image_name)
    {
        $config['upload_path'] = './assets/admin/upload/product/';
        $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
        $config['file_name'] = $file_name;
        $config['overwrite'] = true;
        $config['max_size'] = 10240; // 10MB


        $this->load->library('upload', $config);
        if ($this->upload->do_upload($image_name)) {
            $gbr = $this->upload->data();

            $config['image_library'] = 'gd2';
            $config['source_image'] = './assets/admin/upload/product/' . $gbr['file_name'];
            $config['create_thumb'] = false;
            $config['maintain_ratio'] = false;
            $config['quality'] = '50%';
            $config['new_image'] = './assets/admin/upload/product/' . $gbr['file_name'];
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();


            return $this->upload->data('file_name');
        } else {

            return '0';
        }

    }
}
