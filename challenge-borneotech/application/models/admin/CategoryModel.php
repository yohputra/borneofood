<?php

class CategoryModel extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    public function getData()
    {
        $this->db->select('*');
        // $this->db->where('fdelete', '0');
        // $this->db->order_by('modifiedDate', 'desc');
        $query = $this->db->get('t_category');
        $result_array = $query->result_array();

        return $result_array;
    }

    public function storeData($post)
    {
        $datetime = date('Y-m-d H:i:s');
        if(!empty($post['id'])){
            $update_data = array(
                'name' => $post['name'],
            );
            $this->db->where('id', $post['id']);
            $store = $this->db->update('t_category', $update_data);
        }else{
            $insert_data = array(
                'name' => $post['name']
            );
            $store = $this->db->insert('t_category', $insert_data);
        }
        

        return $store;
    }

    public function getOneData($id)
    {
        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get('t_category');
        $result = $query->row_array();

        return $result;
    }
}
