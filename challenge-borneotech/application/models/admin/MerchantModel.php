<?php

class MerchantModel extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    public function getData()
    {
        $this->db->select('*');
        // $this->db->where('fdelete', '0');
        // $this->db->order_by('modifiedDate', 'desc');
        $query = $this->db->get('t_merchant');
        $result_array = $query->result_array();

        return $result_array;
    }
}
