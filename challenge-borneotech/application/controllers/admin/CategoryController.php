<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CategoryController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct(); 

        $this->load->model('admin/categorymodel');
    }

    public function index()
    {
        $data['row_data'] = $this->categorymodel->getData();
        $content = array(
            'row_data' => $data,
        );
        $content['data_content'] = "category_view";
        $content['content_modal'] = "modal/category_modal_view";

        $this->load->view('template_view', $content);
    }

    public function get($id){
        $data = $this->categorymodel->getOneData($id);
        if (sizeof($data) != 0) {
            $balikan = [
                'status' => '1',
                'message' => 'success',
                'data' => $data
            ];

        } else {
            $balikan = [
                'status' => '0',
                'message' => 'failed',
                'data' => []
            ];
        }
        echo json_encode($balikan);
        
    }

    public function store(){       
        $data = $this->categorymodel->storeData($_POST);
        if ($data == 1) {
            $balikan = [
                'status' => '1',
                'message' => 'success',
                'data' => $data
            ];
        }else {
            $balikan = [
                'status' => '0',
                'message' => 'failed',
                'data' => []
            ];
        }
        echo json_encode($balikan);
    }
}
