<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MerchantController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct(); 

        $this->load->model('admin/merchantmodel');
    }

    public function index()
    {
        $data['row_data'] = $this->merchantmodel->getData();
        $content = array(
            'row_data' => $data,
        );
        $content['data_content'] = "merchant_view";

        $this->load->view('template_view', $content);
    }
}
