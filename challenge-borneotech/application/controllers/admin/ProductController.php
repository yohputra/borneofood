<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ProductController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct(); 
        $this->load->model('admin/productmodel');
        $this->load->model('admin/categorymodel');
        $this->load->model('admin/merchantmodel');
    }

    public function index()
    {
        $product = [];
        $data['row_data'] = $this->productmodel->getData();
        foreach($data['row_data'] as $row){
            $datas = $row;
            $datas['img_path'] = base_url('assets/admin/upload/product/'. $row['img_path']);
            array_push($product, $datas);
        }
        $data['row_data'] = $product;
        $data['category_data'] = $this->categorymodel->getData();
        $data['merchant_data'] = $this->merchantmodel->getData();
        $content = array(
            'row_data' => $data,
        );
    
        $content['data_content'] = "product_view";
        $content['content_modal'] = "modal/product_modal_view";

        $this->load->view('template_view', $content);
    }

    public function store(){       
        $data = $this->productmodel->storeData($_POST);
        if ($data == 1) {
            $balikan = [
                'status' => '1',
                'message' => 'success',
                'data' => $data
            ];
        }else {
            $balikan = [
                'status' => '0',
                'message' => 'failed',
                'data' => []
            ];
        }
        echo json_encode($balikan);
    }
    
    public function get($id){
        $data = $this->productmodel->getOneData($id);
        if (sizeof($data) != 0) {
            $balikan = [
                'status' => '1',
                'message' => 'success',
                'data' => $data
            ];

        } else {
            $balikan = [
                'status' => '0',
                'message' => 'failed',
                'data' => []
            ];
        }
        echo json_encode($balikan);
        
    }
}
