<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class ProductApiController extends REST_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->model('admin/productmodel');
        $this->load->model('admin/categorymodel');
    }


    public function getProductCategory_get()
    {
        $data = $this->categorymodel->getData();
        $balikan = [
            'status' => 1,
            'message' => 'success',
            'data' => $data
        ];
        $this->set_response($balikan, REST_Controller::HTTP_OK); 
    }

    public function getProduct_get()
    {
        $data = $this->productmodel->getData();
        $balikan = [
            'status' => 1,
            'message' => 'success',
            'data' => $data
        ];
        $this->set_response($balikan, REST_Controller::HTTP_OK); 
    }

    public function setFavoriteProduct_post()
    {
        $data = $this->productmodel->storeFavoriteProduct($_POST);
        if ($data == 1) {
            $balikan = [
                'status' => 1,
                'message' => 'success',
                'data' => $data
            ];
        }else {
            $balikan = [
                'status' => 0,
                'message' => 'failed',
                'data' => []
            ];
        }

        $this->set_response($balikan, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

    public function getProductFavorite_get()
    {
        $data = $this->productmodel->getProductFavorite();
        $balikan = [
            'status' => 1,
            'message' => 'success',
            'data' => $data
        ];
        $this->set_response($balikan, REST_Controller::HTTP_OK); 
    }
}
