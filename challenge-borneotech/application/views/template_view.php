<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Borneofood</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/theme/adminlte/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/theme/adminlte/dist/css/AdminLTE.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/theme/adminlte/dist/css/skins/_all-skins.min.css">

  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/theme/adminlte/plugins/datatables/dataTables.bootstrap.css">

  <!-- Parsley CSS -->
  <link href="<?php echo base_url(); ?>assets/theme/adminlte/dist/css/parsley.css" rel="stylesheet">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/theme/adminlte/dist/css/sweetalert2.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/theme/adminlte/dist/css/sweetalert2.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/admin.css">
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->

<body class="hold-transition skin-blue layout-top-nav">
  <div class="wrapper">

    <header class="main-header">
      <nav class="navbar navbar-static-top">
        <div class="container">
          <div class="navbar-header">
            <a href="../../index2.html" class="navbar-brand"><b>Borneofood</b></a>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
              <i class="fa fa-bars"></i>
            </button>
          </div>

          <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
            <ul class="nav navbar-nav">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Master Data <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="<?php echo base_url(); ?>merchant">Merchant</a></li>
                  <li><a href="<?php echo base_url(); ?>category">Category Product</a></li>
                  <li><a href="<?php echo base_url(); ?>">Product</a></li>
                </ul>
              </li>
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>
    </header>
    <!-- Full Width Column -->
    <div class="content-wrapper">
      <div class="container">
        <?php $this->load->view($data_content); ?>
      </div><!-- /.container -->
    </div><!-- /.content-wrapper -->

    <?php if ($content_modal != "") {
      $this->load->view($content_modal);
    } ?>

    <footer class="main-footer">
      <div class="container">

        <strong>Copyright &copy; 2023 All rights reserved.
      </div><!-- /.container -->
    </footer>
  </div><!-- ./wrapper -->

  <div id="rpModal">
    <center>
      <div class="loader"></div>
    </center>
  </div>

  <script>
    var base_url = '<?php echo base_url(); ?>';
  </script>

  <!-- jQuery 2.1.4 -->
  <script src="<?php echo base_url(); ?>assets/theme/adminlte/plugins/jQuery/jQuery-2.1.4.min.js"></script>
  <!-- Bootstrap 3.3.5 -->
  <script src="<?php echo base_url(); ?>assets/theme/adminlte/bootstrap/js/bootstrap.min.js"></script>
  <!-- DataTables -->
  <script src="<?php echo base_url(); ?>assets/theme/adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/theme/adminlte/plugins/datatables/dataTables.bootstrap.min.js"></script>
  <!-- AdminLTE App -->
  <script src="<?php echo base_url(); ?>assets/theme/adminlte/dist/js/app.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="<?php echo base_url(); ?>assets/theme/adminlte/dist/js/demo.js"></script>

  <script src="<?php echo base_url(); ?>assets/admin/js/category.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/js/product.js"></script>

  <!-- Parsley JS -->
  <script src="<?php echo base_url(); ?>assets/theme/adminlte/dist/js/parsley.js"></script>
  <script src="<?php echo base_url(); ?>assets/theme/adminlte/dist/js/parsley.min.js"></script>

  <!-- Sweetalert 2 -->
  <script src="<?php echo base_url(); ?>assets/theme/adminlte/dist/js/sweetalert2.min.js"></script>

  <script>
    $(function() {
      $("#dataTable").DataTable();
    });
  </script>
</body>

</html>