 <!-- Content Header (Page header) -->
 <!-- Main content -->
 <section class="content">
   <div class="box">
     <div class="box-header">
       <h3 class="box-title"> Master Data Merchant</h3>
     </div><!-- /.box-header -->
     <div class="box-body">
       <table id="dataTable" class="table table-bordered table-striped">
         <thead>
           <tr>
             <th>No</th>
             <th>Nama</th>
           </tr>
         </thead>
         <tbody>
           <?php $i = 1;
            foreach ($row_data['row_data'] as $row) : ?>
             <tr>
               <td><?php echo $i; ?></td>
               <td><?php echo $row['name']; ?></td>
             </tr>
           <?php $i++;
            endforeach; ?>
       </table>
     </div><!-- /.box-body -->
   </div><!-- /.box -->
 </section><!-- /.content -->