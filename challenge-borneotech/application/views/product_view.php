 <!-- Content Header (Page header) -->
 <!-- Main content -->
 <section class="content">
   <div class="box">
     <div class="box-header">
       <h3 class="box-title"> Master Data Product</h3>
       <div class="pull-right hidden-xs">
         <button class="btn btn-block btn-info" id="productAdd">Add New</button>
       </div>
     </div><!-- /.box-header -->
     <div class="box-body">
       <table id="dataTable" class="table table-bordered table-striped">
         <thead>
           <tr>
             <th>No</th>
             <th>Merchant</th>
             <th>Category</th>
             <th>Name</th>
             <th>Description</th>
             <th>Image</th>
             <th>Normal Price</th>
             <th>Disc Price</th>
             <th>Action</th>
           </tr>
         </thead>
         <tbody>
           <?php $i = 1;
            foreach ($row_data['row_data'] as $row) : ?>
             <tr>
               <td><?php echo $i; ?></td>
               <td><?php echo $row['name_merchant']; ?></td>
               <td><?php echo $row['name_cat']; ?></td>
               <td><?php echo $row['name']; ?></td>
               <td><?php echo $row['desc']; ?></td>
               <td><img class="imagesize" src="<?php echo $row['img_path']; ?>" /></td>
               <td><?php echo $row['normal_price']; ?></td>
               <td><?php echo $row['disc_price']; ?></td>
               <td align="center" width="10%">
                 <button id="productEdit" class="btn btn-warning margin5 btn-sm" data-value="<?php echo $row['id']; ?>">
                   <i class="fa fa-edit"></i>
                 </button>
               </td>
             </tr>
           <?php $i++;
            endforeach; ?>
       </table>
     </div><!-- /.box-body -->
   </div><!-- /.box -->
 </section><!-- /.content -->