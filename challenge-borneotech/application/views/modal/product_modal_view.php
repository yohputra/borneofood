<div class="modal fade" id="productModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title labelAdmin" id="title_product_modal">Add Product</h3>
      </div>
      <div class="modal-body">
        <form role="form" class="form-horizontal" id="productForm" method="POST" enctype="multipart/form-data">
          <div class="tab-content">
            <div class="col-md-12">
              <input type="hidden" class="form-control" id="id" name="id" value="">

              <div class="box-body">
                <div class="form-group">
                  <label>Name</label>
                  <input type="text" class="form-control" name="name" id="name" placeholder="" required="">
                </div>
                <div class="form-group">
                  <label>Desc</label>
                  <input type="text" class="form-control" name="desc" id="desc" placeholder="" required="">
                </div>
                <div class="form-group">
                  <label>Merchant</label>
                  <select name="merchant_id" id="merchant_id" class="form-control" required="">
                    <option value="">-- Pilih data --</option>
                    <?php foreach ($row_data['merchant_data'] as $row) : ?>
                      <option value="<?php echo $row['id']; ?>"> <?php echo $row['name']; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Category</label>
                  <select name="category_id" id="category_id" class="form-control" required="">
                    <option value="">-- Pilih data --</option>
                    <?php foreach ($row_data['category_data'] as $row) : ?>
                      <option value="<?php echo $row['id']; ?>"> <?php echo $row['name']; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Normal Price</label>
                  <input type="text" class="form-control" name="normal_price" id="normal_price" placeholder="" required="">
                </div>
                <div class="form-group">
                  <label>Disc Price</label>
                  <input type="text" class="form-control" name="disc_price" id="disc_price" placeholder="" required="">
                </div>
                <div class="form-group">
                  <label>Image Product</label>
                  <div style="width:10%;">
                    <label for="image_source" class="form-control">
                      <center>
                        <i class="fa fa-camera" aria-hidden="true"></i>
                      </center>
                    </label>
                  </div>
                  <input type="hidden" class="form-control" id="old_image" name="old_image" value="">
                  <input class="fa fa-camera" style="display: none" type="file" id="image_source" name="image_source" onchange="previewImage();" accept="image/jpeg,image/jpg,image/jpe,image/png,image/gif,image/webp,image/bmp,image/tiff" ref="input" />
                  <div class="preview">
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-9">
                      <img id="preview_image" alt="image preview" />
                    </div>
                  </div>
                </div>
              </div><!-- /.box-body -->

            </div>
          </div>
          <div class="modal-footer" style="justify-content:center;">
            <button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary" id="productSubmit">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>