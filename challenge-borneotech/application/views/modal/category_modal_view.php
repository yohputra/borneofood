<div class="modal fade" id="categoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title labelAdmin" id="title_category_modal">Add Category</h5>
      </div>
      <div class="modal-body">
        <form role="form" class="form-horizontal" id="categoryForm" method="POST" enctype="multipart/form-data">
          <div class="tab-content">
            <div class="col-md-12">
              <input type="hidden" class="form-control" id="id" name="id" value="">

              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="text" class="form-control" name="name" id="name" placeholder="" required="">
                </div>
              </div><!-- /.box-body -->

            </div>
          </div>
          <div class="modal-footer" style="justify-content:center;">
            <button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary" id="categorySubmit">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>