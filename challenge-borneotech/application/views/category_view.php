 <!-- Content Header (Page header) -->
 <!-- Main content -->
 <section class="content">
   <div class="box">
     <div class="box-header">
       <h3 class="box-title"> Master Data Category</h3>
       <div class="pull-right hidden-xs">
         <button class="btn btn-block btn-info" id="categoryAdd">Add New</button>
       </div>
     </div><!-- /.box-header -->
     <div class="box-body">
       <table id="dataTable" class="table table-bordered table-striped">
         <thead>
           <tr>
             <th>No</th>
             <th>Nama</th>
             <th>Action</th>
           </tr>
         </thead>
         <tbody>
           <?php $i = 1;
            foreach ($row_data['row_data'] as $row) : ?>
             <tr>
               <td><?php echo $i; ?></td>
               <td><?php echo $row['name']; ?></td>
               <td align="center" width="10%">
                 <button id="categoryEdit" class="btn btn-warning margin5 btn-sm" data-value="<?php echo $row['id']; ?>">
                   <i class="fa fa-edit"></i>
                 </button>
               </td>
             </tr>
           <?php $i++;
            endforeach; ?>
       </table>
     </div><!-- /.box-body -->
   </div><!-- /.box -->
 </section><!-- /.content -->